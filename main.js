'use strict';
let chbox1 = document.getElementById('checkbox1');
let chbox2 = document.getElementById('checkbox2');
let chbox3 = document.getElementById('checkbox3');
let chbox4 = document.getElementById('checkbox4');
let rangeLength = document.getElementById('length');
let genPassword = document.getElementById('generatedPassword');
let passLength = document.getElementById('lengthValue');
let time = document.getElementById('clock');
let genButton = document.getElementById('myButton');
let truePassword;
let result;

genButton.disabled = true;

function disabledButton() {
  genButton.disabled = true ? !chbox1.checked && !chbox2.checked && !chbox3.checked && !chbox4.checked : false;
}

function getRandomNumber(max) {
  max = Math.floor(Math.random() * truePassword.length);
  return max;
}

function getPassword() {
  const password1 = chbox1.checked ? 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' : "";
  const password2 = chbox2.checked ? '1234567890' : "";
  const password3 = chbox3.checked ? 'abcdefghijklmnopqrstuvwxyz' : "";
  const password4 = chbox4.checked ? '!@#$%^_' : "";

  truePassword = password1 + password2 + password3 + password4;
  result = "";
  while (result.length < rangeLength.value) {
    result += truePassword[getRandomNumber()];
  }

  genPassword.value = result;

}

function copyButton() {
  const copyText = genPassword;
  copyText.select();
  document.execCommand("copy");
}

function showValue() {
  passLength.innerHTML = rangeLength.value;
}

function timer() {
  const timeTo = new Date().toLocaleTimeString();
  time.innerHTML = timeTo;
}

setInterval(() => timer(), 1000);